var birds = 
    [
      {
       "state":"Alabama",
       "bird":"Yellowhammer"
      },
      {
       "state":"Montana",
       "bird":"Western Meadowlark"
      },
      {
       "state":"Alaska",
       "bird":"Willow Ptarmigan"
      },
      {
       "state":"Nebraska",
       "bird":"Western Meadowlark"
      },
      {
       "state":"Arizona",
       "bird":"Cactus Wren Nevada Mountain Bluebird"
      },
      {
       "state":"Arkansas",
       "bird":"Mockingbird"
      },
      {
       "state":"New Hampshire",
       "bird":"Purple Finch"
      },
      {
       "state":"California",
       "bird":"California Valley Quail"
      },
      {
       "state":"New Jersey",
       "bird":"Eastern Goldfinch"
      },
      {
       "state":"Colorado",
       "bird":"ark Bunting"
      },
      {
       "state":"New Mexico",
       "bird":"Roadrunner"
      },
      {
       "state":"Connecticut",
       "bird":"Robin"
      },
      {
       "state":"New York",
       "bird":"Bluebird"
      },
      {
       "state":"Delaware",
       "bird":"Blue Hen Chicken"
      },
      {
       "state":"North",
       "bird":"Carolina Cardinal"
      },
      {
       "state":"Florida",
       "bird": "Mockingbird"
      },
      {
       "state":"North Dakota",
       "bird":"Western Meadowlark"
      },
      {
       "state":"Georgia",
       "bird":"Brown Thrasher"
      },
      {
       "state":"Ohio",
       "bird":"Cardinal"
      },
      {
       "state":"Hawaii",
       "bird":"Nene"
      },
      {
       "state":"Oklahoma",
       "bird":"Scissor-tailed Flycatcher"
      },
      {
       "state":"Idaho",
       "bird":"Mountain Bluebird"
      },
      {
       "state":"Oregon",
       "bird":"Western Meadowlark"
      },
      {
       "state":"Illinois",
       "bird":"Cardinal"
      },
      {
       "state":"Pennsylvania",
       "bird":"Ruffed Grouse"
      },
      {
       "state":"Indiana",
       "bird":"Cardinal"
      },
      {
       "state":"Rhode Island",
       "bird":"Rhode Island Red"
      },
      {
       "state":"Iowa",
       "bird":"Eastern Goldfinch"
      },
      {
       "state":"South",
       "bird":"Carolina Great Carolina Wren"
      },
      {
       "state":"Kansas",
       "bird":"Western Meadowlark"
      },
      {
       "state":"South Dakota",
       "bird":"Ring-necked Pheasant"
      },
      {
       "state":"Kentucky",
       "bird":"Cardinal"
      },
      {
       "state":"Tennessee",
       "bird":"Mockingbird"
      },
      {
       "state":"Louisiana",
       "bird":"Eastern Brown Pelican"
      },
      {
       "state":"Texas",
       "bird":"Mockingbird"
      },
      {
       "state":"Maine",
       "bird":"Chickadee"
      },
      {
       "state":"Utah",
       "bird":"Common American Gull"
      },
      {
       "state":"Maryland",
       "bird":"Baltimore Oriole"
      },
      {
       "state":"Vermont",
       "bird":"Hermit Thrush"
      },
      {
       "state":"Massachusetts",
       "bird":"Chickadee"
      },
      {
       "state":"Virginia",
       "bird":"Cardinal"
      },
      {
       "state":"Michigan",
       "bird":"American Robin"
      },
      {
       "state":"Washington",
       "bird":"Willow Goldfinch"
      },
      {
       "state":"Minnesota",
       "bird":"Common Loon West"
      },
      {
       "state":"Virginia",
       "bird":"Cardinal"
      },
      {
       "state":"Mississippi",
       "bird":"Mockingbird"
      },
      {
       "state":"Wisconsin",
       "bird":"Robin"
      },
      {
       "state":"Missouri",
       "bird":"Bluebird"
      },
      {
       "state":"Wyoming",
       "bird":"estern Meadowlark"
      }
    ];

    module.exports = {
      birds:birds
    }